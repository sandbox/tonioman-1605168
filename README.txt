The BIRT Drupal module allows a BIRT design's HTML contents to be displayed.

BIRT creates a BI Report content types in Drupal. 

It allows dynamic content from any data source to be displayed. 

Anyone with the need to add data-driven BIRT content will want this module.

Drupal 7 version: 

You must have your site's private file directory setup.

The private directory is at /admin/config/media/file-system. 

To get started, just go to /admin/config/birt and enter the Tomcat BIRT server.
